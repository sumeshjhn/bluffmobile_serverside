import random
import card

def dealHands(handSizes):
	random.seed()
	deck = []
	for i in range(52):
		deck.append(i)
		j = random.randint(0,i)
		deck[i], deck[j] = deck[j], deck[i]
	dealtCards = 0
	hands = []
	for handSize in handSizes:
		hand = []
		for i in range(handSize):
			hand.append(card.Card(deck[dealtCards]%13 + 2, deck[dealtCards]/13 + 1))
			dealtCards+=1
		hands.append(hand)
	return hands
	
if __name__ == '__main__':
	print dealHands([4,1,3,4])