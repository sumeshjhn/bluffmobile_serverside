from twisted.internet import reactor
from autobahn.websocket import WebSocketServerFactory, \
                               WebSocketServerProtocol, \
                               listenWS

import sys
import threading
import time
import StringIO
import pdb
import json as simplejson
import base64
import os
import shutil
import subprocess
import random
import json
import deal
import gamelogic    

class Game:
  def __init__(self, gameID, factory):
    self.gameID=gameID
    self.players={} #status 1=playing 0=disconnected
    self.gameInProgress = False
    self.starting = False
    self.numberOfCards=[]
    self.hands=[]
    self.startingPlayer = None
    self.factory=factory
    self.turn=None
    self.currentPlayerHand = {}        # the current rank contents contain: {"player":"Amar", "hand": {place hand here}} player here represents the person who did the highest rank
    self.currentPlayerHand["player"]=None
    self.currentPlayerHand["hand"]=None
    self.isHandPresent=None
    
  def addUser(self,userID):
    if self.gameInProgress == False:
      returnedPlayer = self.players.get(userID,None)
      if returnedPlayer is None:
        tmp = {}
        self.players[userID]=tmp
        self.players[userID]["status"]=1
        return True
      else:
        return False
    else:
      return False

  def removeUser(self,userID): 
    if self.gameInProgress == False:
      del self.players[userID]
    else:
      self.players[userID]["status"]=0
      self.players[userID]["dirty"]=True
      if self.countRemainingPlayers()==0:
        self.restartServer()
      else:
        if self.turn == userID:
          self.checkDisconnectedPlayer()

  def startGameRequest(self):
    if self.gameInProgress == False:
      if self.starting==False:
        self.starting=True
        reactor.callLater(10,self.startGame)

  def endOfRoundRoutine(self):
    users=self.getUserArray()
    for user in users:
      if self.players[user]["status"]==0:
        self.players[user]["dirty"]=False
        self.numberOfCards[users.index(user)]=0

  def playerBluff(self,userID,msg): # TODO: Take a userID string and rank {} and return a json response saying if successful move or rejected
    if self.turn==userID:
      #validate move
      if gamelogic.validateHand(msg['hand'], self.currentPlayerHand['hand'])==True:
        self.currentPlayerHand["player"]=userID
        self.currentPlayerHand["hand"]=msg["hand"]
        return True
      else:
        return False
    else:
      return False

  def getNextPlayer(self,userID): # gives next player with cards ignoring those that disconnected
    users = self.getUserArray()
    myOrder = range(users.index(userID),len(users)) + range(0,users.index(userID))
    users = [ users[i] for i in myOrder]
    for user in users:
      if user!=userID and self.numberOfCards[users.index(user)]!=0: 
          self.turn=user
          return
    print "*********** Internal Failure 1 *******************"

  
  def countRemainingPlayers(self):
    users = self.getUserArray()
    counter=0
    for user in users:
      if self.numberOfCards[users.index(user)]>0 and self.players[user]["status"]==1:
        counter=counter+1
    return counter

  def startNewRound(self):
    if self.isHandPresent == True:
      userID=self.turn
    else:
      userID=self.currentPlayerHand["player"]
    users = self.getUserArray()
    mylist=users
    if self.countRemainingPlayers()>1:
      myOrder = range(users.index(userID),len(users)) + range(0,users.index(userID))
      mylist = [ mylist[i] for i in myOrder]
      for user in mylist:
        if self.numberOfCards[users.index(user)] != 0 and self.players[user]["status"]==1:
          self.startingPlayer=user
          self.turn=user
          self.currentPlayerHand["player"]=None
          self.currentPlayerHand["hand"]=None
          self.isHandPresent=None
          hands = self.getNewHands(users)
          for user in users:
            if self.players[user]["status"]==1:
              self.factory.sendMsgToUser(user,hands[user])
          return
      print "********************************REAL FAIL HERE************************************"
    else:
      response={}
      response["action"]=96
      for user in users:
        if self.numberOfCards[users.index(user)]>0 and self.players[user]["status"]==1:
          response["winner"]=user
      for user in users:
        if self.players[user]["status"]==1:
          self.factory.sendMsgToUser(user,response) 
      self.restartServer()     
  
  def restartServer(self):
    users = self.getUserArray()
    for user in users:
      if self.players[user]["status"]==1:
        self.factory.bootUser(user)
    self.players={} #status 1=playing 0=disconnected
    self.gameInProgress = False
    self.starting = False
    self.numberOfCards=[]
    self.hands=[]
    self.startingPlayer = None
    self.turn=None
    self.currentPlayerHand = {}        # the current rank contents contain: {"player":"Amar", "hand": {place hand here}} player here represents the person who did the highest rank
    self.currentPlayerHand["player"]=None
    self.currentPlayerHand["hand"]=None
    self.isHandPresent=None

  def checkDisconnectedPlayer(self):
    if self.players[self.turn]["status"]==0 and self.players[self.turn]["dirty"]==True:
      if self.currentPlayerHand==None:
        self.getNextPlayer(self.turn)
        self.broadcastCurrentHand()
        return True
      else:
        args={}
        args["player"]=self.currentPlayerHand["player"]
        if self.playerCall(self.turn,args):
          self.broadcastCall()
          self.endOfRoundRoutine()
          self.startNewRound()
          return True
        else:
          print "*********** Internal Failure 2 *******************"
    else:
      return False

  def playerCall(self,userID,msg): # TODO: Take a userID string and rank {} and return a json response saying if successful move or rejected
    if self.currentPlayerHand["player"]!=None:
      if self.turn==userID:
        #validate move
        if msg["player"]==self.currentPlayerHand["player"]:
          playerHands=[]
          users = self.getUserArray()
          if gamelogic.isHandPresent(self.currentPlayerHand["hand"], self.hands) == True:
            self.isHandPresent=True
            self.numberOfCards[users.index(userID)]=self.numberOfCards[users.index(userID)]-1
            return True
          else:
            self.isHandPresent=False
            self.numberOfCards[users.index(msg["player"])]=self.numberOfCards[users.index(msg["player"])]-1
            return True
        else:
          return False

      else:
        return False
    else:
      return False

  def startGame(self):
    if len(self.players)>1:
      self.gameInProgress=True
      for x in range(0,len(self.players)):
        self.numberOfCards.append(5)
      users = self.getUserArray()
      self.startingPlayer=users[0]
      self.turn=users[0]
      hands = self.getNewHands(users)
      for user in users:
        if self.players[user]["status"]==1:
          self.factory.sendMsgToUser(user,hands[user])
    else:
      reactor.callLater(10,self.startGame)  

  def getUserArray(self):
    users = []
    for player in self.players:
      users.append(player)
    return users

  def broadcastCurrentHand(self):
    response={}
    response["hand"]=self.currentPlayerHand["hand"]
    response["action"]=98
    response["handOwner"]=self.currentPlayerHand["player"]
    response["player"]=self.turn
    users = self.getUserArray()
    for user in users:
      if self.players[user]["status"]==1:
        self.factory.sendMsgToUser(user,response)

  def broadcastCall(self):
    response={}
    response["action"]=97
    response["calledPlayer"]=self.currentPlayerHand["player"]
    response["callingPlayer"]=self.turn
    response["isHandPresent"]=self.isHandPresent
    users = self.getUserArray()
    for user in users:
      if self.players[user]["status"]==1:
        self.factory.sendMsgToUser(user,response)

  def getNewHands(self,users):
    self.hands=deal.dealHands(self.numberOfCards)
    messages={}  
    players=[]
    for user in users:
      tmp={}
      tmp["name"]=user
      tmp["cards"]=self.numberOfCards[users.index(user)]
      players.append(tmp)
    for hand in self.hands:
      userID=users[self.hands.index(hand)]
      playerHand=[]
      for card in hand:
        tmp={}
        tmp["rank"] = card.rank
        tmp["suit"] = card.suit
        playerHand.append(tmp)
      response={}
      response["action"]=99
      response["player"]=self.startingPlayer
      response["cards"]=playerHand
      response["players"]=players
      if userID==self.startingPlayer:
        response["isUserTurn"]=True
      else:
        response["isUserTurn"]=False
      messages[userID]=response
    return messages

class BroadcastServerProtocol(WebSocketServerProtocol):

  def onOpen(self):
    print "connected "+self.peerstr

  def onMessage(self, msg, binary):
    self.factory.parseMsg(msg,self)

  def connectionLost(self, reason):
    WebSocketServerProtocol.connectionLost(self, reason)
    print "connection lost" 
    self.factory.unregister(self)

class BroadcastServerFactory(WebSocketServerFactory):
  
  def __init__(self, url, debug = False, debugCodePaths = False):
    WebSocketServerFactory.__init__(self, url, debug = debug, debugCodePaths = debugCodePaths)
    self.clients = {}
    self.games=[]
    for x in range(0, 3):
      self.games.append(Game(x,self))

  def parseMsg(self,msg,client):
    response=json.loads(msg)
    print "client response: "
    print response
    if response['action']==1:
      self.registerUser(response,client)
    elif response['action']==2:
      self.playerBluff(response,client)
    elif response['action']==3:
      self.playerCall(response,client)
    elif response['action']==4:
      self.getGames(client)
    elif response['action']==5:
      self.startGameRequest(client)

  def playerCall(self,msg,client):
    userID=self.getUserID(client)
    response={}
    if self.games[self.clients[userID]["gameID"]].playerCall(userID,msg) == True:
      response["response"]=1
      self.sendMsg(client,response)
      self.games[self.clients[userID]["gameID"]].broadcastCall()
      self.games[self.clients[userID]["gameID"]].endOfRoundRoutine()
      self.games[self.clients[userID]["gameID"]].startNewRound()
    else:
      response["response"]=0
      self.sendMsg(client,response)

  def playerBluff(self,msg,client):
     userID=self.getUserID(client)
     response={}
     if self.games[self.clients[userID]["gameID"]].playerBluff(userID,msg) == True:
      response["response"]=1
      self.sendMsg(client,response)
      self.games[self.clients[userID]["gameID"]].getNextPlayer(userID)
      if self.games[self.clients[userID]["gameID"]].checkDisconnectedPlayer() == False:
        self.games[self.clients[userID]["gameID"]].broadcastCurrentHand()
     else: 
      response["response"]=0
      self.sendMsg(client,response)
     
  def startGameRequest(self,client):
    response=[]
    userID=self.getUserID(client)
    if userID is not None:
      self.games[self.clients[userID]["gameID"]].startGameRequest()

  def getGames(self,client):
    response=[]
    for game in self.games:
      response.append(game.gameID)
    self.sendMsg(client,response)

  def getUserID(self,client):
    for user in self.clients:
      if self.clients[user]["client"]==client:
        return user
    return None    

  def registerUser(self,msg,client):
    response = {}
    name=msg["name"]
    returnedClient = self.clients.get("name",None)
    if returnedClient is None:
      if self.games[msg["gameID"]].addUser(name) == True:
        userObject={}
        userObject["gameID"]=msg["gameID"]
        userObject["client"]=client
        self.clients[name]=userObject
        response["response"]=1
      else:
        response["response"]=0
    else:
      response["response"]=0
    response["action"]=1
    self.sendMsg(client,response)

  def unregister(self, client):
    userID=self.getUserID(client)
    if userID is not None:
      self.games[self.clients[userID]["gameID"]].removeUser(userID)
      del self.clients[userID]
      print "unregistered client " + client.peerstr

  def sendMsg(self,client,msg):
    print json.dumps(msg)
    client.sendMessage(json.dumps(msg))

  def sendMsgToUser(self,userID,msg):
    print json.dumps(msg)
    self.clients[userID]["client"].sendMessage(json.dumps(msg))

  def bootUser(self,userID):
    self.clients[userID]["client"].sendClose()
    del self.clients[userID]

if __name__ == '__main__':  
   print "starting"
   ServerFactory = BroadcastServerFactory
   factory = ServerFactory("ws://localhost:9000",
                           debug = False,
                           debugCodePaths = False)

   factory.protocol = BroadcastServerProtocol
   listenWS(factory)
   reactor.run()