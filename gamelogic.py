import card

class Suit:
	EMPTY = 0
	MINVALUE = 0
	MINREALVALUE = 1
	MAXVALUE = 4
		
class Rank:
	EMPTY = 0
	MINVALUE = 2
	MAXVALUE = 14
	
class HandType:
	EMPTY = 0
	SINGLE = 1
	PAIR = 2
	TWOPAIR = 3
	TRIPLE = 4
	STRAIGHT = 5
	FLUSH = 6
	FULLHOUSE = 7
	QUAD = 8
	STRAIGHTFLUSH = 9
	MINVALUE = 1
	MAXVALUE = 9
	
def isLegalHand(hand):
	if 'suit' not in hand or 'handType' not in hand or 'ranks' not in hand:
		return False
	suit = hand['suit']
	handType = hand['handType']
	ranks = hand['ranks']
	isFlush = handType in (HandType.FLUSH , HandType.STRAIGHTFLUSH)
	if suit < Suit.MINVALUE or suit > Suit.MAXVALUE\
			or handType < HandType.MINVALUE or handType > HandType.MAXVALUE\
			or suit == Suit.EMPTY and isFlush or suit != Suit.EMPTY and not isFlush\
		  or not isinstance(ranks, list):
		return False
	currentMin = Rank.MAXVALUE + 1
	for rank in ranks:
		if rank < Rank.MINVALUE or rank > Rank.MAXVALUE:
			return False
		if rank >= currentMin and handType != HandType.FULLHOUSE:
			return False
		currentMin = rank
		
	return handType in (HandType.SINGLE, HandType.PAIR, HandType.TRIPLE, HandType.QUAD) and len(ranks) == 1\
			or handType in (HandType.STRAIGHT, HandType.STRAIGHTFLUSH) and len(ranks) == 1 and ranks[0] >= Rank.MINVALUE + 4\
		  or handType in (HandType.TWOPAIR, HandType.FULLHOUSE) and len(ranks) == 2\
			or handType == HandType.FLUSH and len(ranks) == 4
	
def isBetterHand(newHand, oldHand):
	if oldHand == None:
		return True
	oldRanks = oldHand['ranks']
	newRanks = newHand['ranks']
	newHandType = newHand['handType']
	if (newHandType > oldHand['handType']):
		return True
	elif (newHandType < oldHand['handType']):
		return False
		
	if newHandType in (HandType.SINGLE, HandType.PAIR, HandType.TRIPLE, 
			HandType.QUAD, HandType.STRAIGHT, HandType.STRAIGHTFLUSH):
			return newRanks[0] > oldRanks[0]
	elif newHandType in (HandType.TWOPAIR, HandType.FULLHOUSE):
		return newRanks[0] > oldRanks[0] or newRanks[0] == oldRanks[0]\
				and newRanks[1] > oldRanks[1]	
	elif newHandType == HandType.FLUSH:
			return newRanks[0] > oldRanks[0] or newRanks[0] == oldRanks[0]\
					and newRanks[1] > oldRanks[1]	or newRanks[0] == oldRanks[0]\
					and newRanks[1] == oldRanks[1]	and newRanks[2] > oldRanks[2]\
					or newRanks[0] == oldRanks[0] and newRanks[1] == oldRanks[1]\
					and newRanks[2] == oldRanks[2] and newRanks[3] > oldRanks[3]

def isHandPresent(statedHand, playerHands):
	availableRanksInSuit = [0] * 13  
	availableRanks = [0] * 13
	numavailableRanksInSuit = 0
	handType = statedHand['handType']
	suit = statedHand['suit'] - Suit.MINREALVALUE
	ranks = []
	
	for rank in statedHand['ranks']:
		ranks.append(rank - Rank.MINVALUE)
		
	for playerHand in playerHands:
		for card in playerHand:
			availableRanks[card.rank - Rank.MINVALUE]+= 1
			if card.suit == statedHand['suit']:
				numavailableRanksInSuit+= 1
				availableRanksInSuit[card.rank - Rank.MINVALUE] = 1
			
	if handType == HandType.SINGLE:
		return availableRanks[ranks[0]] > 0
	elif handType == HandType.PAIR:
		return availableRanks[ranks[0]] > 1
	elif handType == HandType.TWOPAIR:
		return availableRanks[ranks[0]] > 1 and availableRanks[ranks[1]] > 1
	elif handType == HandType.TRIPLE:
		return availableRanks[ranks[0]] > 2
	elif handType == HandType.STRAIGHT:
		for i in range(ranks[0] - 4, ranks[0] + 1):
			if availableRanks[i] == 0:
				return False
	elif handType == HandType.FLUSH:
		if numavailableRanksInSuit < 5:
			return False
		for rank in ranks:
			if availableRanksInSuit[rank] == 0:
				return False
	elif handType == HandType.FULLHOUSE:
		return availableRanks[ranks[0]] > 2 and availableRanks[ranks[0]] > 1
	elif handType == HandType.QUAD:
		return availableRanks[ranks[0]] == 4
	elif handType == HandType.STRAIGHTFLUSH:
		for i in range(ranks[0] - 4, ranks[0] + 1):
			if availableRanksInSuit[i] == 0:
				return False
	return True
	
def validateHand(newHand, oldHand):
	return isLegalHand(newHand) and isBetterHand(newHand, oldHand)
	
if __name__ == '__main__':
	hand = {}
	hand['handType'] = 6
	hand['suit'] = 1
	hand['ranks'] = [10, 9, 7, 2]
	print validateHand(hand, hand)
	print isHandPresent(hand, [[card.Card(10,1), card.Card(9,1)],\
	[card.Card(7,1), card.Card(8,1), card.Card(2,1)]])